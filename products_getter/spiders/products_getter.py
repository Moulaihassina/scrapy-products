import scrapy
from ..items import ProductsGetterItem

class PSpider(scrapy.Spider):
    name = "products_getter"
    start_urls = ['https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas']

    def parse(self, response):        
        # get the products names
        for item in response.css("div.shelfProductTile-information header.shelfProductTile-description a::text"):
            product = ProductsGetterItem()
            product['name'] = item.extract()
            # get the menu / breadcrumbs
            breadcrumbs_list = response.css("li.breadcrumbs-link span a.ng-star-inserted::text").extract()
            product['menu'] = breadcrumbs_list
                
            yield product

